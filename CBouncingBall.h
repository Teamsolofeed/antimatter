#ifndef __CBOUNCINGBALL_H__
#define __CBOUNCINGBALL_H__

#include "GameSprite.h"
#include "cocos2d.h"
#include "Definitions.h"

USING_NS_CC;

class BouncingBall : public GameSprite{
private:
	int m_Scale;
	void initBouncingBall();
public:
	static BouncingBall* createSprite(GameLayer* layer, Point point);
	BouncingBall(GameLayer* layer, Point point);
	~BouncingBall();
	void enlarge();
	void reduce();
};


#endif
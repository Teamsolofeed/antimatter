#ifndef __SPLASH_SCENE_H__
#define __SPLASH_SCENE_H__

#include "Definitions.h"
#include "cocos2d.h"
#include "MainMenuScene.h"
USING_NS_CC;

class SplashScene : public Layer{
public:
	static Scene* createScene();

	SplashScene();

	//virtual bool init();

	CREATE_FUNC(SplashScene);

	void loadingCallBack(Texture2D* texture);
private:
	void GoToMainMenuScene(float dt);
	Label *m_labelLoading;
	Label *m_labelPercent;
	int m_numberOfSprites;
	int m_numberOfLoadedSprites;
};

#endif // __SPLASH_SCENE_H__
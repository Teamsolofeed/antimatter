#include "GameSprite.h"
#include "GameLayer.h"

GameSprite::GameSprite(GameLayer* layer, Point point, int Type){
	m_vector = Vec2(0, 0);

	m_startPosition = point;

	m_Layer = layer;

	m_type = Type;
}

//Deconstructor
GameSprite::~GameSprite(void){

}

void GameSprite::setPosition(const Point& pos) {
	//set sprite position
	Sprite::setPosition(pos);

	//set position for body
	if (m_Body) {
		m_Body->SetTransform(b2Vec2(pos.x / PTM_RATIO, pos.y / PTM_RATIO), m_Body->GetAngle());
	}
}

//radius equals half the width
float GameSprite::radius() {
	return getTexture()->getContentSize().width * 0.5f;
}

//apply Force to the body
void GameSprite::applyForce(b2Vec2 touchBegin, b2Vec2 touchEnd){
	//if the game runs slow convert float to int
	b2Vec2 force = b2Vec2(0, 0);
	force += touchBegin;
	force -= touchEnd;
	auto size = Director::getInstance()->getWinSize();
	float scale = 1 / (size.height / 2);
	float temp = FORCESCALE * scale;
	force *= FORCESCALE * scale;

	//auto temp = force.Normalize();
	m_Body->ApplyForceToCenter(force, false);
}

void GameSprite::updateScore(){
	int score = m_Layer->getScore();
	Label* label = m_Layer->getLabel();
	char tmp[10];
	score++; //update score
	m_Layer->setScore(score);
	sprintf(tmp, "%d", score);
	label->setString(tmp);
}

int GameSprite::getType(){
	return m_type;
}

void GameSprite::update(float dt){
	if (m_Body){
		//update the sprite to its body
		setPositionX(m_Body->GetPosition().x * PTM_RATIO);
		setPositionY(m_Body->GetPosition().y * PTM_RATIO);
		setRotation(CC_RADIANS_TO_DEGREES(-1 * m_Body->GetAngle()));
	}
}

int GameSprite::getCurrentScore(){
	return m_Layer->getScore();
}
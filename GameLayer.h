#ifndef __GAMELAYER_H__
#define __GAMELAYER_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "CBouncingBall.h"
#include "CNormalBall.h"
#include "Definitions.h"
#include "MyContactListener.h"

//divide layer into categories
enum{
	backGroundLayer,
	playGroundLayer,
	foreGroundLayer
};

class GameLayer : public cocos2d::Layer{
private:
	Size m_winSize;
	NormalBall* m_ball;
	GameSprite* m_groundData;
	CCArray* m_BouncingBalls;
	CCArray* m_NormalBalls;
	CCArray* m_AbilityBalls;
	Sprite* m_normalBallSprite;
	Sprite* m_ballNoti;
	Size m_visibleSize;
	Vec2 m_origin;
	int m_type;
	int m_nextType;
	int m_currentAvailBalls;
	b2Vec2 m_touchBegin;
	b2Vec2 m_touchEnded;
	void createGameScene();
	bool m_tutorialFlag;
	bool m_hasShownInstruction;
	Label* m_tutorialInstruct;
	int m_tutorialBallNumber;
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	CC_SYNTHESIZE(b2World*, m_World, World);

	CC_SYNTHESIZE(unsigned int, m_score, Score);

	CC_SYNTHESIZE(Label*, m_scoreLabel, Label);

	CC_SYNTHESIZE(Label*, m_ballNumber, BallNotiLabel);

	void initPhysics();

	void update(float dt);

	void createGround();

	~GameLayer();

	bool onTouchBegan(Touch* touch, Event* event);

	void onTouchMoved(Touch* touch, Event* event);

	void onTouchEnded(Touch* touch, Event* event);

	void ApplyForce(BouncingBall* bouncingball, NormalBall* abilityball);

	void updateNotification();

	void removeBalls();

	/* implement the "static create()" method manually it will call our init function
	the purpose of this macro is to call autorelease(), so you dont have to remember to free
	this variable later*/
	CREATE_FUNC(GameLayer);
};
#endif

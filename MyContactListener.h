#ifndef __MYCONTACTLISTENER_H__
#define __MYCONTACTLISTENER_H__
#include "Box2D\Box2D.h"
#include "GameSprite.h"
#include "GameOverScene.h"
#include "SimpleAudioEngine.h"
#include "CBouncingBall.h"
using namespace CocosDenshion;

class MyContactListener : public b2ContactListener{
	void BeginContact(b2Contact* contact){
		//check if this was the egg
		void* bodyUserDataA = contact->GetFixtureA()->GetBody()->GetUserData();
		void* bodyUserDataB = contact->GetFixtureB()->GetBody()->GetUserData();
		if (bodyUserDataA && bodyUserDataB){
			GameSprite* gameSpriteA = static_cast<GameSprite*>(bodyUserDataA);
			GameSprite* gameSpriteB = static_cast<GameSprite*>(bodyUserDataB);
			int typeA = gameSpriteA->getType();
			int typeB = gameSpriteB->getType();

			//if one of them is the egg and the other is the animal
			if ((typeA == NORMALBALL && typeB > 0) || (typeA > 0 && typeB == NORMALBALL)){
				gameSpriteA->updateScore();
				//handle the ability balls
				if (typeA == PUSHBALL_ACTIVE){
					gameSpriteA->setType(PUSHBALL_DISABLE);
					return;
				}
				if (typeA == PULLBALL_ACTIVE){
					gameSpriteA->setType(PULLBALL_DISABLE);
					return;
				}
				if (typeA == DOUBLEBALL_ACTIVE) {
					gameSpriteA->setType(DOUBLEBALL_DISABLE);
					UserDefault *def = UserDefault::getInstance();
					def->setIntegerForKey("NEWBOUNCE", 1);
					def->flush();
					return;
				}
				if (typeA == ENLARGE_ACTIVE) {
					gameSpriteA->setType(ENLARGE_DISABLE);
					BouncingBall* bounceBall = static_cast<BouncingBall*>(bodyUserDataB);
					bounceBall->enlarge();
					return;
				}
				if (typeA == SHRINK_ACTIVE) {
					gameSpriteA->setType(SHRINK_DISABLE);
					BouncingBall* bounceBall = static_cast<BouncingBall*>(bodyUserDataB);
					bounceBall->reduce();
					return;
				}
				if (typeB == PUSHBALL_ACTIVE){
					gameSpriteB->setType(PUSHBALL_DISABLE);
					return;
				}
				if (typeB == PULLBALL_ACTIVE){
					gameSpriteB->setType(PULLBALL_DISABLE);
					return;
				}
				if (typeB == DOUBLEBALL_ACTIVE) {
					gameSpriteB->setType(DOUBLEBALL_DISABLE);
					UserDefault *def = UserDefault::getInstance();
					def->setIntegerForKey("NEWBOUNCE", 1);
					def->flush();
					return;
				}
				if (typeB == SHRINK_ACTIVE) {
					gameSpriteB->setType(SHRINK_DISABLE);
					BouncingBall* bounceBall = static_cast<BouncingBall*>(bodyUserDataA);
					bounceBall->reduce();
					return;
				}
				if (typeB == ENLARGE_ACTIVE) {
					gameSpriteB->setType(ENLARGE_DISABLE);
					BouncingBall* bounceBall = static_cast<BouncingBall*>(bodyUserDataA);
					bounceBall->enlarge();
					return;
				}
				//handle normal balls
				if (typeA == NORMALBALL){
					gameSpriteB->setType(-1); //if hit this block again dont update score
				}
				else{
					gameSpriteA->setType(-1);
				}
			}
			//if touch the ground which has id -2 move to game over scene
			else if ((typeA == NORMALBALL && typeB == GROUND) || (typeA == GROUND && typeB == NORMALBALL)){
				auto scene = GameOverScene::createScene(gameSpriteA->getCurrentScore());
				// if you use SimpleAudioEngine, it must be pause
				SimpleAudioEngine::getInstance()->stopBackgroundMusic();
				Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
			}
		}
	}
};

#endif

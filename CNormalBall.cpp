#include "CNormalBall.h"
#include "GameLayer.h"

//free memory
NormalBall::~NormalBall(){

}

//constructor
NormalBall::NormalBall(GameLayer* layer, Point point, int type) : GameSprite(layer, point, type){
	//TODO: add animation
	m_streak = MotionStreak::create(1, 2, 32, Color3B::WHITE, "streak.png");
	m_Layer->addChild(m_streak, playGroundLayer);
}

NormalBall* NormalBall::createSprite(GameLayer* layer, Point point, int Type){
	NormalBall* newNormalBall = new NormalBall(layer, point, Type);

	//create sprite name
	char sprite_name[20] = { 0 };
	sprintf(sprite_name, "brick%d.png", Type);

	//create type name
	char type_name[2] = { 0 };
	sprintf(type_name, "%d", Type);
	//newAnimal->setName(type_name);

	//create sprite
	if (newNormalBall && newNormalBall->initWithFile(sprite_name)){
		auto size = newNormalBall->getContentSize();
		//create physic body
		newNormalBall->initNormalBall();

		newNormalBall->autorelease(); //automatically free memory when not use 

		//newNormalBall->setScale(scale);

		//add to the layer
		layer->addChild(newNormalBall, playGroundLayer);
		return newNormalBall;
	}

	//if fail free memory
	CC_SAFE_DELETE(newNormalBall);
	return newNormalBall = nullptr;
}

void NormalBall::fadeInAnimation(){
	auto action = FadeTo::create(0.5, 0);
	auto sequence = Sequence::create(action, RemoveSelf::create(), NULL);
	runAction(sequence);
}

void NormalBall::initNormalBall(){
	//create box2d body
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody; //dynamic body

	//add body to the world
	m_Body = m_Layer->getWorld()->CreateBody(&bodyDef);
	m_Body->SetSleepingAllowed(true); //to increase performance

	//define shape
	b2PolygonShape box;
	box.SetAsBox(NORMALBALLSIZE/PTM_RATIO, NORMALBALLSIZE/PTM_RATIO);

	//define fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = BOUNCEBALL_WEIGHT*.8f;
	fixtureDef.restitution = 1;

	//add fixture to the body
	m_Body->CreateFixture(&fixtureDef);

	//set userdata to animal
	m_Body->SetUserData(this);

	//set position
	setPosition(m_startPosition);
}

void NormalBall::update(float dt){
	if (m_Body){
		//update the sprite to its body
		setPositionX(m_Body->GetPosition().x * PTM_RATIO);
		setPositionY(m_Body->GetPosition().y * PTM_RATIO);
		setRotation(CC_RADIANS_TO_DEGREES(-1 * m_Body->GetAngle()));
		m_streak->setPosition(m_Body->GetPosition().x * PTM_RATIO, m_Body->GetPosition().y * PTM_RATIO);
	}
}

#include "SplashScene.h"


SplashScene::SplashScene() : m_numberOfSprites(12),
m_numberOfLoadedSprites(0){
	auto size = Director::getInstance()->getWinSize();

	m_labelLoading = Label::createWithTTF("Loading...", "fonts/arial.ttf", 15);
	m_labelPercent = Label::createWithTTF("%0", "fonts/arial.ttf", 15);

	m_labelLoading->setPosition(Vec2(size.width / 2, size.height / 2 - 20));
	m_labelPercent->setPosition(Vec2(size.width / 2, size.height / 2 + 20));

	this->addChild(m_labelLoading);
	this->addChild(m_labelPercent);

	//load textures
	Director::getInstance()->getTextureCache()->addImageAsync("BGI.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("ball.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("Background.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("Play Button Clicked.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("Play Button.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("Title.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("brick1.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("brick2.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("brick3.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("brick4.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("brick5.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
	Director::getInstance()->getTextureCache()->addImageAsync("brick6.png", CC_CALLBACK_1(SplashScene::loadingCallBack, this));
}

void SplashScene::loadingCallBack(Texture2D* texture){
	++m_numberOfLoadedSprites;
	char tmp[10];
	sprintf(tmp, "%%%d", (int)(((float)m_numberOfLoadedSprites / m_numberOfSprites) * 100));
	m_labelPercent->setString(tmp);

	if (m_numberOfLoadedSprites == m_numberOfSprites){
		//this->removeChild(m_labelLoading, true);
		//this->removeChild(m_labelPercent, true);
		this->scheduleOnce(schedule_selector(SplashScene::GoToMainMenuScene), DISPLAY_TIME_SPLASH_SCENE);
	}
}

void SplashScene::GoToMainMenuScene(float dt){
	auto scene = MainMenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}

Scene* SplashScene::createScene(){
	//scene is an autorelease object
	auto scene = Scene::create();

	//layer is an autoerlease object
	auto layer = SplashScene::create();

	//add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}




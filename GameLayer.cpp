#include "GameLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;
MyContactListener myContactListenerInstance;

GameLayer::~GameLayer(){
	//free memory
	CC_SAFE_DELETE(m_World);
	CC_SAFE_DELETE(m_BouncingBalls);
	CC_SAFE_DELETE(m_NormalBalls);
	CC_SAFE_DELETE(m_AbilityBalls);
	delete m_groundData;
}

//init game's scene
Scene* GameLayer::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameLayer::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameLayer::init()
{
	// 1. super init first
	if (!Layer::init()){
		return false;
	}

	//for positioning purposes
	m_winSize = Director::getInstance()->getWinSize();
	m_origin = Director::getInstance()->getVisibleOrigin();
	m_visibleSize = Director::getInstance()->getVisibleSize();

	//init bouncing ball capacity
	m_BouncingBalls = CCArray::createWithCapacity(NUMBEROFBOUNCINGBALLS);
	m_BouncingBalls->retain();

	//init number of normal balls
	m_NormalBalls = CCArray::createWithCapacity(NUMBEROFNORMALBALLS);
	m_NormalBalls->retain();

	//init number of ability balls
	m_AbilityBalls = CCArray::createWithCapacity(NUMBEROFABILITYBALLS);
	m_AbilityBalls->retain();

	//init listener
	auto listener = EventListenerTouchOneByOne::create();

	//CC_CALLBACK_2 is to call the function with two parameters
	listener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(GameLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	//init tutorial flag
	UserDefault *def = UserDefault::getInstance();
	m_tutorialFlag = def->getIntegerForKey("TUTORIAL", 1);
	def->flush();
	m_hasShownInstruction = false;

	//init gameScene
	createGameScene();

	//update the world
	this->scheduleUpdate();
	return true;
}

void GameLayer::createGameScene(){
	//create background Image
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	
	auto backgroundSprite = Sprite::create("In Game Background.png");
	backgroundSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));

	this->addChild(backgroundSprite, backGroundLayer);

	//create background music
	//SimpleAudioEngine::getInstance()->playBackgroundMusic("BGM.mp3", true);

	//init score board
	m_score = 0;
	m_scoreLabel = Label::createWithTTF("0", "fonts/arial.ttf", m_visibleSize.height * SCORE_FONT_SIZE);
	m_scoreLabel->setColor(Color3B::BLACK);
	m_scoreLabel->setPosition(m_winSize.width * 0.5f, m_winSize.height * 0.75f);
	this->addChild(m_scoreLabel, backGroundLayer);

	//init ball notification
	//init ball notification
	if (m_tutorialFlag) {
		m_type = 3; //if this is the first time playing
		m_tutorialBallNumber = 4; //give user the feeling of the game first
	}
	else {
		m_type = RandomHelper::random_int(1, 6);
		m_tutorialBallNumber = 0;
	}

	char sprite_name[20] = { 0 };
	sprintf(sprite_name, "brick%d.png", m_type);
	m_ballNoti = Sprite::create(sprite_name);
	m_ballNoti->setPosition(m_origin.x + m_ballNoti->getContentSize().width, m_origin.y + m_ballNoti->getContentSize().height);
	this->addChild(m_ballNoti, backGroundLayer);
	m_currentAvailBalls = NUMBEROFAVAILABLEBALLS; //init number of avaiblebal balls
	char tmp[6] = { 0 };
	sprintf(tmp, "x %d", NUMBEROFAVAILABLEBALLS);
	m_ballNumber = Label::createWithTTF(tmp, "fonts/arial.ttf", m_visibleSize.height * SCORE_FONT_SIZE/2);
	m_ballNumber->setColor(Color3B::WHITE);
	m_ballNumber->setPosition(m_origin.x + m_ballNoti->getContentSize().width * 2 + 5, m_origin.y + m_ballNoti->getContentSize().height);
	this->addChild(m_ballNumber);

	//init tutorial 
	m_tutorialInstruct = Label::createWithTTF("slide down \nto make matter", "fonts/arial.ttf", m_visibleSize.height * SCORE_FONT_SIZE / 2);
	m_tutorialInstruct->setPosition(m_winSize.width / 2, m_winSize.height / 2 - m_scoreLabel->getContentSize().height - 10);
	this->addChild(m_tutorialInstruct, foreGroundLayer);

	//init physics
	initPhysics();
}

//create physic world
void GameLayer::initPhysics(){
	//create gravity
	b2Vec2 gravity;
	gravity.Set(0.0f, .0f);

	//init world and add gravity to the world
	m_World = new b2World(gravity);

	//add other options
	m_World->SetAllowSleeping(true);
	m_World->SetContinuousPhysics(true);

	//add contact listener
	m_World->SetContactListener(&myContactListenerInstance);

	//create ground
	createGround();

	//create bouncing ball
	Point startingPoint = Vec2(m_winSize.width * 0.5f, m_winSize.height * 0.5f);
	BouncingBall* bouncingball = BouncingBall::createSprite(this, startingPoint);

	//add to egg array
	m_BouncingBalls->addObject(bouncingball);
}

//create game boundary
void GameLayer::createGround(){
	//define the ground body
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0);

	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	b2Body* groundBody = m_World->CreateBody(&groundBodyDef);
	b2Body* groundBodyBottom = m_World->CreateBody(&groundBodyDef);

	//define the ground box shape
	b2EdgeShape groundBox;

	//bottom ground
	groundBox.Set(b2Vec2(0, 0), b2Vec2(m_winSize.width / PTM_RATIO, 0));
	groundBodyBottom->CreateFixture(&groundBox, 0); //shape and density

	//set user data for bottom ground
	m_groundData = new GameSprite(this, Point(0, 0), -2);
	groundBodyBottom->SetUserData(m_groundData);

	//top ground
	groundBox.Set(b2Vec2(0, m_winSize.height / PTM_RATIO), b2Vec2(m_winSize.width / PTM_RATIO, m_winSize.height / PTM_RATIO));
	groundBody->CreateFixture(&groundBox, 0);

	//left
	groundBox.Set(b2Vec2(0, 0), b2Vec2(0, m_winSize.height / PTM_RATIO));
	groundBody->CreateFixture(&groundBox, 0);

	//right
	groundBox.Set(b2Vec2(m_winSize.width / PTM_RATIO, 0), b2Vec2(m_winSize.width / PTM_RATIO, m_winSize.height / PTM_RATIO));
	groundBody->CreateFixture(&groundBox, 0);
}

void GameLayer::updateNotification() {
	//init random type for the next ball
	if (m_tutorialBallNumber) {
		m_nextType = 3;
		m_tutorialBallNumber--;
	}
	else {
		do {
			m_nextType = RandomHelper::random_int(1, 6);
		} while (m_AbilityBalls->count() == NUMBEROFABILITYBALLS && (m_type == 1 || m_type == 2));
	}

	//update ball notification
	this->removeChild(m_ballNoti);
	char sprite_name2[20] = { 0 };
	sprintf(sprite_name2, "brick%d.png", m_nextType);
	m_ballNoti = Sprite::create(sprite_name2);
	m_ballNoti->setPosition(m_origin.x + m_ballNoti->getContentSize().width, m_origin.y + m_ballNoti->getContentSize().height);
	this->addChild(m_ballNoti, backGroundLayer);
}

bool GameLayer::onTouchBegan(Touch* touch, Event* event){
	//remove the instruction
	if (m_hasShownInstruction == false) {
		b2Vec2 gravity = b2Vec2(0, -1); //set gravity
		m_World->SetGravity(gravity);
		m_tutorialInstruct->runAction(FadeTo::create(0.5, 0));
		m_hasShownInstruction = true;
	}

	//if pause then tap to resume
	if (Director::getInstance()->isPaused() == true) {
		m_tutorialInstruct->runAction(FadeTo::create(0.5, 0));
		Director::getInstance()->resume();
		b2Vec2 force = m_touchBegin - m_touchEnded;
		force *= 0.05;
		m_ball->getBody()->ApplyLinearImpulse(force, m_ball->getBody()->GetLocalCenter(), true);
		UserDefault *def = UserDefault::getInstance();
		def->setIntegerForKey("TUTORIAL", 0); //just play the tutorial once only
		def->flush();

		//remove child
		this->removeChild(m_tutorialInstruct);
		return false;
	}

	//update ball sprite in the notification
	updateNotification();
	if (m_currentAvailBalls == 0) {
		removeBalls();
		m_currentAvailBalls = NUMBEROFAVAILABLEBALLS;
	}

	char updateBall[6] = { 0 };
	//create sprite name
	char sprite_name1[20] = { 0 };
	sprintf(sprite_name1, "brick%d.png", m_type);

	//create sprite and set the position
	m_normalBallSprite = Sprite::create(sprite_name1);
	m_normalBallSprite->setPosition(touch->getLocation().x, touch->getLocation().y);
	this->addChild(m_normalBallSprite, playGroundLayer);

	//on touch began position
	m_touchBegin = b2Vec2(touch->getLocation().x, touch->getLocation().y);

	//update the current available balls
	m_currentAvailBalls--;
	sprintf(updateBall, "x %d", m_currentAvailBalls);
	m_ballNumber->setString(updateBall);

	return true;
}

void GameLayer::onTouchMoved(Touch* touch, Event* event){
	m_normalBallSprite->setPosition(touch->getLocation().x, touch->getLocation().y);
}

void GameLayer::onTouchEnded(Touch* touch, Event* event){
	//avoid player tapping to create matters
	b2Vec2 touchEnd = b2Vec2(touch->getLocation().x, touch->getLocation().y);
	b2Vec2 disVec = m_touchBegin - touchEnd;
	float distance = disVec.Length();
	if (distance <= 2) {
		this->removeChild(m_normalBallSprite);
		return;
	}

	NormalBall* m_normal_ball = NormalBall::createSprite(this, touch->getLocation(), m_type);
	if (m_type == 1 || m_type == 2 || m_type == DOUBLEBALL_ACTIVE){
		m_AbilityBalls->addObject(m_normal_ball);
	}
	else{
		m_NormalBalls->addObject(m_normal_ball);
	}
	this->removeChild(m_normalBallSprite);

	//applyForce
	m_normal_ball->applyForce(m_touchBegin, touchEnd);

	//instruct the ability ball if needed
	if ((m_type == 1 || m_type == 2) && m_tutorialFlag == 1) {
		Director::getInstance()->pause(); //pause the game
		m_tutorialInstruct = Label::createWithTTF("Some matters have ability.\n Like this one", "fonts/arial.ttf", m_visibleSize.height * SCORE_FONT_SIZE / 2);
		m_tutorialInstruct->setPosition(m_winSize.width / 2, m_winSize.height / 2 - m_scoreLabel->getContentSize().height - 10);
		m_tutorialInstruct->runAction(FadeIn::create(10));
		this->addChild(m_tutorialInstruct);
		m_tutorialFlag = 0;

		//just for the tutorial
		m_ball = m_normal_ball;
		m_touchEnded = touchEnd;
	}

	m_type = m_nextType;
}

//apply appropriate force to the bouncing ball
void GameLayer::ApplyForce(BouncingBall* bouncingball, NormalBall* abilityball){
	//find the center of the bouncing ball
	b2Vec2 bouncing_ball_position = bouncingball->getBody()->GetWorldCenter();

	//find the center of ability ball
	b2Vec2 normal_ball_position = abilityball->getBody()->GetWorldCenter();

	//caculate the force 
	b2Vec2 distance = bouncing_ball_position - normal_ball_position;
	float force = 42.0*BOUCINGBALLSIZE / distance.LengthSquared();
	distance.Normalize(); //convert to unit vector

	//convert force to vector
	b2Vec2 F = force * distance;

	//check the type for the ability
	if (abilityball->getType() == 1){ //pushing
		bouncingball->getBody()->ApplyForce(F, bouncing_ball_position, true);
	}
	else if (abilityball->getType() == 2){ //pulling
		bouncingball->getBody()->ApplyForce(-F, bouncing_ball_position, true);
	}
}

//update the game
void GameLayer::update(float dt){
	m_World->Step(dt, 8, 3);
	BouncingBall* bouncingball;
	NormalBall* ball;

	//create object
	CCObject* BouncingBallObject;
	CCObject* AbilityBallObject;
	CCObject* NormalBallObject;

	//loop through bouncing balls
	CCARRAY_FOREACH(m_BouncingBalls, BouncingBallObject){
		bouncingball = static_cast<BouncingBall*>(BouncingBallObject);

		//find the center of the bouncing balls
		b2Vec2 bouncing_ball_position = bouncingball->getBody()->GetWorldCenter();

		//loop through ability balls
		if (m_AbilityBalls->count() != 0){ //if there are ability balls
			CCARRAY_FOREACH(m_AbilityBalls, AbilityBallObject){
				ball = static_cast<NormalBall*>(AbilityBallObject);

				ApplyForce(bouncingball, ball);

				ball->update(dt);
			}
		}
		bouncingball->update(dt);
	}

	//update other normal balls
	CCARRAY_FOREACH(m_NormalBalls, NormalBallObject) {
		ball = static_cast<NormalBall*>(NormalBallObject);
		ball->update(dt);
	}

	UserDefault *def = UserDefault::getInstance();

	bool createNewBounceBall = def->getIntegerForKey("NEWBOUNCE", 0);

	if (createNewBounceBall)
	{
		if (m_BouncingBalls->count() < NUMBEROFBOUNCINGBALLS)
		{
			float ranW = CCRANDOM_0_1();
			float ranH = 1 - CCRANDOM_0_1()*.5f;
			Point startingPoint = Vec2(m_winSize.width * ranW, m_winSize.height * ranH);
			BouncingBall* bouncingball = BouncingBall::createSprite(this, startingPoint);

			//add to bouncing balls array
			m_BouncingBalls->addObject(bouncingball);
		}
		def->setIntegerForKey("NEWBOUNCE", 0);
	}

	def->flush();
}

//remove ball and physics body
void GameLayer::removeBalls() {
	NormalBall* ball;
	CCObject* ballObject;

	if (m_NormalBalls->count() != 0) {
		CCARRAY_FOREACH(m_NormalBalls, ballObject) {
			ball = static_cast<NormalBall*>(ballObject);

			//destroy physic body
			m_World->DestroyBody(ball->getBody());

			//destroy sprite with fade transition
			ball->fadeInAnimation();
		}
		m_NormalBalls->removeAllObjects();
	}

	if (m_AbilityBalls->count() != 0) {
		CCARRAY_FOREACH(m_AbilityBalls, ballObject) {
			ball = static_cast<NormalBall*>(ballObject);

			//destroy physic body
			m_World->DestroyBody(ball->getBody());

			//destroy sprite with fade transition
			ball->fadeInAnimation();
		}
		m_AbilityBalls->removeAllObjects();
	}
}
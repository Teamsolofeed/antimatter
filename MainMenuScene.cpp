#include "MainMenuScene.h"
#include "GameLayer.h"
#include "Definitions.h"

USING_NS_CC;

Scene* MainMenuScene::createScene(){
	auto scene = Scene::create();

	auto layer = MainMenuScene::create();

	scene->addChild(layer);

	return scene;
}

bool MainMenuScene::init(){
	if (!Layer::init()){
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto backgroundSprite = Sprite::create("Background.png");
	backgroundSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));

	/****
	Size bgSize = backgroundSprite->getContentSize();
	float scale = visibleSize.height / (float)bgSize.height;

	backgroundSprite->setScale(scale);
	****/
	this->addChild(backgroundSprite);
	/**********
	auto titleSprite = Sprite::create("Title.png");
	titleSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height - titleSprite->getContentSize().height));
	//titleSprite->setScale(scale);
	this->addChild(titleSprite);
	************/

	auto playItem = MenuItemImage::create("Play Button.png", "Play Button Clicked.png", CC_CALLBACK_1(MainMenuScene::GotoGameScene, this));
	//playItem->setScale(scale);
	playItem->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height*.4f + origin.y));

	auto menu = Menu::create(playItem, NULL);
	menu->setPosition(Point::ZERO);
	this->addChild(menu);

	return true;
}

void MainMenuScene::GotoGameScene(Ref* sender){
	auto scene = GameLayer::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}
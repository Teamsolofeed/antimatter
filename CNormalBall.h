#ifndef __CNORMALBALL_H__
#define __CNORMALBALL_H__

#include "GameSprite.h"
#include "cocos2d.h"
#include "Definitions.h"
USING_NS_CC;

class NormalBall : public GameSprite{
private:
	void initNormalBall();
	MotionStreak* m_streak;
public:
	static NormalBall* createSprite(GameLayer* layer, Point point, int Type);
	NormalBall(GameLayer* layer, Point point, int type);
	void fadeInAnimation();
	void update(float dt);
	~NormalBall();
};

#endif
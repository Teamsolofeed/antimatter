#ifndef __GAMESPRITE_H__
#define __GAMESPRITE_H__


#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "Definitions.h"

class GameLayer;

using namespace cocos2d;

class GameSprite : public Sprite {
public:
	CC_SYNTHESIZE(Vec2, m_vector, Vector);

	CC_SYNTHESIZE(Point, m_startPosition, Point);

	CC_SYNTHESIZE(b2Body*, m_Body, Body);

	CC_SYNTHESIZE(GameLayer*, m_Layer, Layer);

	CC_SYNTHESIZE(int, m_type, Type);

	GameSprite(GameLayer* layer, Point Point, int Type);

	virtual ~GameSprite();

	virtual void setPosition(const Vec2& pos) override;

	virtual void applyForce(b2Vec2 touchBegin, b2Vec2 touchEnd);

	virtual float radius();

	int getType();

	void updateScore();

	int getCurrentScore();

	virtual void update(float dt);
};

#endif // __GAMESPRITE_H__
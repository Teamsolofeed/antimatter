#include "CBouncingBall.h"
#include "GameLayer.h"

//deconstructor
BouncingBall::~BouncingBall(){
	//free memory
	//CC_SAFE_DELETE(m_EggAnimate);
}

void BouncingBall::enlarge()
{	
	if (m_Scale < BOUNCE_SCALELIMIT)
	{
		m_Scale++;
		auto action = ScaleBy::create(TRANSITION_TIME*.8f, 1.3f);
		this->runAction(action);
	}
}

void BouncingBall::reduce()
{	
	if (m_Scale > -BOUNCE_SCALELIMIT)
	{
		m_Scale--;
		auto action = ScaleBy::create(TRANSITION_TIME*.8f, 0.7f);
		this->runAction(action);
		//m_Body->GetFixtureList()->GetDensity();
	}
}

//constructor
BouncingBall::BouncingBall(GameLayer* layer, Point point) : GameSprite(layer, point, 0){
	m_Scale = 0;
}

BouncingBall* BouncingBall::createSprite(GameLayer* layer, Point point){
	BouncingBall *newBouncingBall = new BouncingBall(layer, point);

	//if create sprite successfully
	if (newBouncingBall && newBouncingBall->initWithFile("ball.png")){
		//create physic body
		newBouncingBall->initBouncingBall();

		newBouncingBall->autorelease(); //automatically free memory when not use
		//newBouncingBall->setScale(scale);

		layer->addChild(newBouncingBall, playGroundLayer);

		return newBouncingBall;
	}

	//if fail free memory
	CC_SAFE_DELETE(newBouncingBall);
	return newBouncingBall = nullptr;
}

void BouncingBall::initBouncingBall(){
	//create box2d body
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody; //dynamic body

	//add body to the world
	m_Body = m_Layer->getWorld()->CreateBody(&bodyDef);
	m_Body->SetSleepingAllowed(true); //to increase perfomance

	//define shape in physical world
	b2PolygonShape box;
	box.SetAsBox(BOUCINGBALLSIZE / PTM_RATIO, BOUCINGBALLSIZE / PTM_RATIO);

	//define fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = BOUNCEBALL_WEIGHT;
	fixtureDef.restitution = 1;

	//add fixture to the body
	m_Body->CreateFixture(&fixtureDef);

	//set userdata to egg
	m_Body->SetUserData(this);

	//set position
	setPosition(m_startPosition);
}
